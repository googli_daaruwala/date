from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from .forms import UserRegister, UserLogin
from django.contrib.auth.models import User, auth
# Create your views here.

def register(request):    
    if request.method == 'POST':
        form = UserRegister(request.POST)
        if form.is_valid():
            form.save()
            return redirect('user-login')
        else:
            return redirect('user-register')
    else:
        form = UserRegister()
        context = {
            'form':form
        }
        return render(request, 'accounts/register.html', context)

def login(request):
    form = UserLogin()
    context = {
        'form':form
    }
    if request.method == 'POST':
        form = UserLogin(request.POST)
        obj = get_object_or_404(User)
        user = auth.authenticate(request, username=obj.username, password= obj.password)
        if user is not None:
            auth.login(request, user)
            print('logged in')
            return redirect('user-register')
    else:
        return render(request, 'accounts/login.html', context)